const player = document.querySelector('.player');
const select = document.querySelectorAll('.select')
const bot = document.querySelectorAll('.bot');
const rps = document.querySelectorAll('.rps');
const com = document.querySelector('.com');
const value = document.querySelector('.status');
const refresh = document.querySelector('.refresh');


player.addEventListener('click', (ev) => {
    const getRandom = Math.floor(Math.random() * 3);
    const playerChoice = ev.target.dataset.number;

    for (let b = 0; b < rps.length; b++) {
        if (b == getRandom) {
            rps[b].classList.add('box-rps');
        }
    }

    for (let a = 0; a < select.length; a++) {
        if (a == playerChoice) {
            select[a].classList.add('box-rps');
        }
    }

    function win() {
        value.classList.add('box-win')
        value.classList.remove('status')
        value.textContent = 'PLAYER 1 WIN'
    }

    function lose() {
        value.classList.add('box-win')
        value.classList.remove('status')
        value.textContent = 'COM WIN'
    }

    if (playerChoice > getRandom) {
        win();
    } else if (playerChoice == 1 && getRandom == 2) {
        win();
    } else if (playerChoice < getRandom) {
        lose();
    } else if (playerChoice == 2 && getRandom == 1) {
        lose();
    } else if (playerChoice == getRandom) {
        value.classList.add('box-draw')
        value.classList.remove('status')
        value.textContent = 'DRAW'
    }

}, {
    once: true
});

refresh.addEventListener('click', (ev) => {
    window.setTimeout(() => {
        window.location.reload(true)
    }, 100)
})