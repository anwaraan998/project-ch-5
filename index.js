import express from 'express'
import {fileURLToPath} from 'url'
import path from 'path';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename)
const app = express();
const port = 3000;

app.use(express.static(__dirname));

app.get('/home', (req, res) => {
    res.sendFile(path.join(__dirname + '/src/pages/home.html'));
});

app.get('/game', (req, res) => {
    res.sendFile(__dirname + '/src/pages/game.html');
})
app.listen(port, () => {
    console.log(`Server is Running on Port: ${port}`)
})

